﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Inventario.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Inventario.Models
{
    public class InventarioContext : DbContext
    {
        string connectionString = "Data Source=.\\INVENTARIO.db";

        public InventarioContext (DbContextOptions<InventarioContext> options)
            : base(options)
        {
        }

        public DbSet<Inventario.Models.Bienes> Bienes { get; set; }
        public DbSet<Inventario.Models.Compras> Compras { get; set; }

        /// <summary>
        /// Obtiene todos los registros de los bienes
        /// </summary>
        /// <returns>Arreglo Json de los bienes</returns>
        public IEnumerable<Bienes> GetAllBienes()
        {
            List<Bienes> bienesList = new List<Bienes>();
            using (SqliteConnection con = new SqliteConnection(connectionString))
            {
                SqliteCommand cmd = new SqliteCommand("SELECT * FROM BIENES", con);
                con.Open();

                SqliteDataReader dr = cmd.ExecuteReader();

                if (!dr.HasRows)
                {
                    return null;
                }

                while (dr.Read())
                {
                    Bienes bien = new Bienes();
                    bien.IdBien = Convert.ToInt32(dr["ID_BIEN"].ToString());
                    bien.IdTipoBien = Convert.ToInt32(dr["ID_TIPO_BIEN"].ToString());
                    bien.NombreBien = dr["NOMBRE_BIEN"].ToString();
                    bien.Serial = dr["SERIAL"].ToString();

                    bienesList.Add(bien);
                }
                con.Close();
            }

            return bienesList;
        }

        /// <summary>
        /// Obtiene un registro de los bienes
        /// </summary>
        /// <param name="id">Llave primaria de los bienes</param>
        /// <returns>un Json de del registro de los bienes seleccionada por id</returns>
        public Bienes GetBien(int? id)
        {
            Bienes bien = new Bienes();
            using (SqliteConnection con = new SqliteConnection(connectionString))
            {
                SqliteCommand cmd = new SqliteCommand("SELECT * FROM BIENES WHERE ID_BIEN = @ID_BIEN", con);

                cmd.Parameters.AddWithValue("@ID_BIEN", id);
                con.Open();

                SqliteDataReader dr = cmd.ExecuteReader();

                if (!dr.HasRows)
                {
                    return null;
                }

                while (dr.Read())
                {
                    bien.IdBien = Convert.ToInt32(dr["ID_BIEN"].ToString());
                    bien.IdTipoBien = Convert.ToInt32(dr["ID_TIPO_BIEN"].ToString());
                    bien.NombreBien = dr["NOMBRE_BIEN"].ToString();
                    bien.Serial = dr["SERIAL"].ToString();
                }
                con.Close();
                dr.Close();

                return bien;
            }
        }

        /// <summary>
        /// Crea un registro en la tablas bienes
        /// </summary>
        /// <remarks>
        /// Ejemplo:
        /// {
        /// "nombreBien": "pasabocas",
        /// "idTipoBien": 3,
        /// "serial": "987654"
        /// }
        /// </remarks>
        /// <param name="bien">Json que tiene todos los campos de la tablas bienes a excepción del ID_COMPRA dado que es un autonumérico</param>
        public void createBien(Bienes bien)
        {
            using (SqliteConnection con = new SqliteConnection(connectionString))
            {
                SqliteCommand cmd = new SqliteCommand("INSERT INTO BIENES ( NOMBRE_BIEN, ID_TIPO_BIEN, SERIAL ) VALUES ( @NOMBRE_BIEN, @ID_TIPO_BIEN, @SERIAL )", con);
                
                cmd.Parameters.AddWithValue("@ID_TIPO_BIEN", bien.IdTipoBien);
                cmd.Parameters.AddWithValue("@NOMBRE_BIEN", bien.NombreBien);
                cmd.Parameters.AddWithValue("@SERIAL", bien.Serial);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        /// <summary>
        /// Actualiza un registro en la tablas bienes
        /// </summary>
        /// <remarks>
        /// Ejemplo:
        /// {
        /// "idBien": 2,
        /// "nombreBien": "Azucar",
        /// "idTipoBien": 3,
        /// "serial": "654321"
        /// }
        /// </remarks>
        /// <param name="bien">Json que tiene todos los campos de la tablas bienes</param>
        public void updateBien(Bienes bien)
        {
            using (SqliteConnection con = new SqliteConnection(connectionString))
            {
                SqliteCommand cmd = new SqliteCommand("UPDATE BIENES SET NOMBRE_BIEN = @NOMBRE_BIEN, ID_TIPO_BIEN = @ID_TIPO_BIEN, SERIAL = @SERIAL WHERE ID_BIEN = @ID_BIEN", con);

                cmd.Parameters.AddWithValue("@ID_BIEN", bien.IdBien);
                cmd.Parameters.AddWithValue("@ID_TIPO_BIEN", bien.IdTipoBien);
                cmd.Parameters.AddWithValue("@NOMBRE_BIEN", bien.NombreBien);
                cmd.Parameters.AddWithValue("@SERIAL", bien.Serial);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        /// <summary>
        /// Elimina un registro de la tablas bienes
        /// </summary>
        /// <param name="id">Valor de la llave primaria del registro que se desea eliminar</param>
        public void deleteBien(int? id)
        {
            using (SqliteConnection con = new SqliteConnection(connectionString))
            {
                SqliteCommand cmd = new SqliteCommand("DELETE FROM BIENES WHERE ID_BIEN = @ID_BIEN", con);

                cmd.Parameters.AddWithValue("@ID_BIEN", id);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }


        /// <summary>
        /// Obtiene todos los registros de las compras
        /// </summary>
        /// <returns>Arreglo Json de las compras</returns>
        public IEnumerable<Compras> GetAllCompras()
        {
            List<Compras> ComprasList = new List<Compras>();
            using (SqliteConnection con = new SqliteConnection(connectionString))
            {
                SqliteCommand cmd = new SqliteCommand("SELECT * FROM VIEW_COMPRAS", con);
                con.Open();

                SqliteDataReader dr = cmd.ExecuteReader();

                if (!dr.HasRows)
                {
                    return null;
                }

                while (dr.Read())
                {
                    Compras compra = new Compras();

                    compra.IdCompra = Convert.ToInt32(dr["ID_COMPRA"].ToString());
                    compra.IdBien = Convert.ToInt32(dr["ID_BIEN"].ToString());
                    compra.NombreBien = dr["NOMBRE_BIEN"].ToString();
                    compra.ValorCompra = Convert.ToDouble(dr["VALOR_COMPRA"].ToString());
                    compra.IdTipoBien = Convert.ToInt32(dr["ID_TIPO_BIEN"].ToString());
                    compra.NombreTipoBien = dr["NOMBRE_TIPO_BIEN"].ToString();
                    compra.FechaCompra = dr["FECHA_COMPRA"].ToString();
                    compra.IdTipoAsignacion = Convert.ToInt32(dr["ID_TIPO_ASIGNACION"].ToString());
                    compra.NombreTipoAsignacion = dr["NOMBRE_TIPO_ASIGNACION"].ToString();

                    ComprasList.Add(compra);
                }
                con.Close();
            }

            return ComprasList;
        }

        /// <summary>
        /// Obtiene un registro de la compra
        /// </summary>
        /// <param name="id">Llave primaria de la compra</param>
        /// <returns>un Json de del registro de la compra seleccionada por id</returns>
        public Compras GetCompra(int? id)
        {
            Compras compra = new Compras();
            using (SqliteConnection con = new SqliteConnection(connectionString))
            {
                SqliteCommand cmd = new SqliteCommand("SELECT * FROM VIEW_COMPRAS WHERE ID_COMPRA = @ID_COMPRA", con);

                cmd.Parameters.AddWithValue("@ID_COMPRA", id);
                con.Open();

                SqliteDataReader dr = cmd.ExecuteReader();

                if (!dr.HasRows)
                {
                    return null;
                }

                while (dr.Read())
                {
                    compra.IdCompra = Convert.ToInt32(dr["ID_COMPRA"].ToString());
                    compra.IdBien = Convert.ToInt32(dr["ID_BIEN"].ToString());
                    compra.NombreBien = dr["NOMBRE_BIEN"].ToString();
                    compra.ValorCompra = Convert.ToDouble(dr["VALOR_COMPRA"].ToString());
                    compra.IdTipoBien = Convert.ToInt32(dr["ID_TIPO_BIEN"].ToString());
                    compra.NombreTipoBien = dr["NOMBRE_TIPO_BIEN"].ToString();
                    compra.FechaCompra = dr["FECHA_COMPRA"].ToString();
                    compra.IdTipoAsignacion = Convert.ToInt32(dr["ID_TIPO_ASIGNACION"].ToString());
                    compra.NombreTipoAsignacion = dr["NOMBRE_TIPO_ASIGNACION"].ToString();
                }
                con.Close();
                dr.Close();

                return compra;
            }
        }

        /// <summary>
        /// Crea un registro en la tabla compras
        /// </summary>
        /// <remarks>
        /// Ejemplo:
        /// {
        /// "idBien": 2,
        /// "valorCompra": 15.0,
        /// "fechaCompra": "2020/01/01",
        /// "idTipoAsignacion": 2
        /// }
        /// </remarks>
        /// <param name="compra">Json que tiene todos los campos de la tabla compras a excepción del ID_COMPRA dado que es un autonumérico</param>
        public void createCompra(Compras compra)
        {
            using (SqliteConnection con = new SqliteConnection(connectionString))
            {
                SqliteCommand cmd = new SqliteCommand("INSERT INTO COMPRAS ( ID_BIEN, VALOR_COMPRA, FECHA_COMPRA, ID_TIPO_ASIGNACION ) VALUES ( @ID_BIEN, @VALOR_COMPRA, @FECHA_COMPRA, @ID_TIPO_ASIGNACION )", con);
                
                cmd.Parameters.AddWithValue("@ID_BIEN", compra.IdBien);
                cmd.Parameters.AddWithValue("@VALOR_COMPRA", compra.ValorCompra);
                cmd.Parameters.AddWithValue("@FECHA_COMPRA", compra.FechaCompra);
                cmd.Parameters.AddWithValue("@ID_TIPO_ASIGNACION", compra.IdTipoAsignacion);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        /// <summary>
        /// Actualiza un registro en la tabla compras
        /// </summary>
        /// <remarks>
        /// Ejemplo:
        /// {
        /// "idCompra": 1,
        /// "idBien": 2,
        /// "valorCompra": 15.0,
        /// "fechaCompra": "2020/01/01",
        /// "idTipoAsignacion": 2
        /// }
        /// </remarks>
        /// <param name="compra">Json que tiene todos los campos de la tabla compras</param>
        public void updateCompra(Compras compra)
        {
            using (SqliteConnection con = new SqliteConnection(connectionString))
            {
                SqliteCommand cmd = new SqliteCommand("UPDATE COMPRAS SET ID_BIEN = @ID_BIEN, VALOR_COMPRA = @VALOR_COMPRA, FECHA_COMPRA = @FECHA_COMPRA, ID_TIPO_ASIGNACION = @ID_TIPO_ASIGNACION WHERE ID_COMPRA = @ID_COMPRA", con);

                cmd.Parameters.AddWithValue("@ID_COMPRA", compra.IdCompra);
                cmd.Parameters.AddWithValue("@ID_BIEN", compra.IdBien);
                cmd.Parameters.AddWithValue("@VALOR_COMPRA", compra.ValorCompra);
                cmd.Parameters.AddWithValue("@FECHA_COMPRA", compra.FechaCompra);
                cmd.Parameters.AddWithValue("@ID_TIPO_ASIGNACION", compra.IdTipoAsignacion);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        /// <summary>
        /// Elimina un registro de la tabla compras
        /// </summary>
        /// <param name="id">Valor de la llave primaria del registro que se desea eliminar</param>
        public void deleteCompra(int? id)
        {
            using (SqliteConnection con = new SqliteConnection(connectionString))
            {
                SqliteCommand cmd = new SqliteCommand("DELETE FROM COMPRAS WHERE ID_COMPRA = @ID_COMPRA", con);

                cmd.Parameters.AddWithValue("@ID_COMPRA", id);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }
    }
}
