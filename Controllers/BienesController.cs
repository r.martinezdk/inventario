﻿using Inventario.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventario.Controllers
{
    //var swaggerUi

    [Route("api/[controller]")]
    [ApiController]
    public class BienesController : ControllerBase
    {
        private readonly InventarioContext _context;

        public BienesController(InventarioContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Obtiene todos los registros de los bienes
        /// </summary>
        /// <returns>Arreglo Json de los bienes</returns>
        [HttpGet]
        public IEnumerable<Bienes> GetBienes()
        {
            return _context.GetAllBienes();
        }

        /// <summary>
        /// Obtiene un registro de los bienes
        /// </summary>
        /// <param name="id">Llave primaria de los bienes</param>
        /// <returns>un Json de del registro de los bienes seleccionada por id</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBienes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Bienes bien = _context.GetBien(id);

            if (bien == null)
            {
                return NotFound();
            }

            return Ok(bien);
        }

        /// <summary>
        /// Actualiza un registro en la tablas bienes
        /// </summary>
        /// <remarks>
        /// Ejemplo Json con data:
        /// {
        /// "idBien": 2,
        /// "nombreBien": "Azucar",
        /// "idTipoBien": 3,
        /// "serial": "654321"
        /// }
        /// </remarks>
        /// <param name="id">Llave primaria de la compra</param>
        /// <param name="bienes">Json que tiene todos los campos de la tabla compras</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBienes([FromRoute] int id, [FromBody] Bienes bienes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bienes.IdBien)
            {
                return BadRequest();
            }

            _context.Entry(bienes).State = EntityState.Modified;

            try
            {
                _context.updateBien(bienes);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BienesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Crea un registro en la tablas bienes
        /// </summary>
        /// <remarks>
        /// Ejemplo:
        /// {
        /// "nombreBien": "pasabocas",
        /// "idTipoBien": 3,
        /// "serial": "987654"
        /// }
        /// </remarks>
        /// <param name="bien">Json que tiene todos los campos de la tabla compras a excepción del ID_COMPRA dado que es un autonumérico</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostBienes([FromBody] Bienes bien)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.createBien(bien);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBienes", new { id = bien.IdBien }, bien);
        }

        /// <summary>
        /// Elimina un registro de la tabla bienes
        /// </summary>
        /// <param name="id">Valor de la llave primaria del registro que se desea eliminar</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBienes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bienes = _context.GetBien(id);
            if (bienes == null)
            {
                return NotFound();
            }

            _context.deleteBien(id);
            await _context.SaveChangesAsync();

            return Ok(bienes);
        }

        private bool BienesExists(int id)
        {
            return _context.Bienes.Any(e => e.IdBien == id);
        }
    }
}