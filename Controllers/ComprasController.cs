﻿using Inventario.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventario.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComprasController : ControllerBase
    {
        private readonly InventarioContext _context;

        public ComprasController(InventarioContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Obtiene todos los registros de las compras
        /// </summary>
        /// <returns>Arreglo Json de las compras</returns>
        [HttpGet]
        public IEnumerable<Compras> GetCompras()
        {
            return _context.GetAllCompras();
        }

        /// <summary>
        /// Obtiene un registro de la compra
        /// </summary>
        /// <param name="id">Llave primaria de la compra</param>
        /// <returns>un Json de del registro de la compra seleccionada por id</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCompras([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Compras compra = _context.GetCompra(id);

            if (compra == null)
            {
                return NotFound();
            }

            return Ok(compra);
        }

        /// <summary>
        /// Actualiza un registro en la tabla compras
        /// </summary>
        /// <remarks>
        /// Ejemplo:
        /// {
        /// "idCompra": 1,
        /// "idBien": 2,
        /// "valorCompra": 15.0,
        /// "fechaCompra": "2020/01/01",
        /// "idTipoAsignacion": 2
        /// }
        /// </remarks>
        /// <param name="id">Llave primaria de la compra</param>
        /// <param name="compra">Json que tiene todos los campos de la tabla compras</param>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCompras([FromRoute] int id, [FromBody] Compras compra)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != compra.IdCompra)
            {
                return BadRequest();
            }

            _context.Entry(compra).State = EntityState.Modified;

            try
            {
                _context.updateCompra(compra);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ComprasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Crea un registro en la tabla compras
        /// </summary>
        /// <remarks>
        /// Ejemplo:
        /// {
        /// "idBien": 2,
        /// "valorCompra": 15.0,
        /// "fechaCompra": "2020/01/01",
        /// "idTipoAsignacion": 2
        /// }
        /// </remarks>
        /// <param name="compra">Json que tiene todos los campos de la tabla compras a excepción del ID_COMPRA dado que es un autonumérico</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostCompras([FromBody] Compras compra)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.createCompra(compra);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCompras", new { id = compra.IdCompra }, compra);
        }

        /// <summary>
        /// Elimina un registro de la tabla compras
        /// </summary>
        /// <param name="id">Valor de la llave primaria del registro que se desea eliminar</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCompras([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var compra = _context.GetCompra(id);
            if (compra == null)
            {
                return NotFound();
            }

            _context.deleteCompra(id);
            await _context.SaveChangesAsync();

            return Ok(compra);
        }

        private bool ComprasExists(int id)
        {
            return _context.Compras.Any(e => e.IdCompra == id);
        }
    }
}