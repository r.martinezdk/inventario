﻿using System.ComponentModel.DataAnnotations;

namespace Inventario.Models
{
    public class Bienes
    {
        [Key]
        public int IdBien { get; set; }

        public string NombreBien { get; set; }

        public int IdTipoBien { get; set; }

        public string Serial { get; set; }
    }
}
