﻿using System.ComponentModel.DataAnnotations;

namespace Inventario.Models
{
    public class Compras
    {
        [Key]
        public int IdCompra { get; set; }
        public int IdBien { get; set; }
        public string NombreBien { get; set; }
        public double ValorCompra { get; set; }
        public int IdTipoBien { get; set; }
        public string NombreTipoBien { get; set; }
        public string FechaCompra { get; set; }
        public int IdTipoAsignacion { get; set; }
        public string NombreTipoAsignacion { get; set; }
    }
}
